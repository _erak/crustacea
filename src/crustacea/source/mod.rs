/**
 * This file is part of crustacea.
 *
 * Copyright (C) 2016 by Erik Kundt <bitshift@posteo.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use std::fmt::Debug;
use std::fmt;

// ----------------------------------------------------------------------------

pub trait Source {

}

impl Debug for Box<Source + 'static> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Source {{  }}")
    }
}

pub struct TextureBlending {
    images: Vec<String>,
}

impl TextureBlending {
    pub fn new() -> TextureBlending {
        TextureBlending {
            images: vec![]
        }
    }
}

impl Source for TextureBlending {

}


