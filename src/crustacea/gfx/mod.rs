/**
 * This file is part of crustacea.
 *
 * Copyright (C) 2017 by Erik Kundt <bitshift@posteo.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

extern crate image;

use sdl2::gfx::primitives::DrawRenderer;
use sdl2::pixels;
use sdl2::render::Canvas;
use sdl2::render::RenderTarget;
use sdl2::video::{Window, WindowContext};

use crustacea::Surface;
use crustacea::utils;

//-----------------------------------------------------------------------------

pub fn draw(canvas: &Canvas<Window>, surface: &Surface) {
    
    let color = pixels::Color::RGB(255, 255, 255);
    let xy_slices = utils::XYSlices::new(&surface.points);
    let vx = xy_slices.vx();
    let vy = xy_slices.vy();

    // if surface.points().len() == 4 {
    //     canvas.rectangle();
    // } else {
    canvas.aa_polygon(&vx, &vy, color);
    // }    

    if surface.selected {
        for point in surface.points() {
            canvas.aa_circle(point.x() as i16, point.y() as i16, 10 as i16, color);
        }
    }


}

