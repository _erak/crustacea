/**
 * This file is part of crustacea.
 *
 * Copyright (C) 2016 by Erik Kundt <bitshift@posteo.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
mod crustacea;

#[macro_use]
extern crate find_folder;
extern crate sdl2;

#[macro_use(lift)]
extern crate carboxyl;

use carboxyl::{Sink, Signal};

use std::time::Duration;

use sdl2::event::Event;
use sdl2::pixels;
use sdl2::keyboard::Keycode;
use sdl2::gfx::primitives::DrawRenderer;
use sdl2::rect::{Rect, Point};
use sdl2::video::FullscreenType;

// use drag_controller::{ DragController, Drag };
use crustacea::gfx;
use crustacea::surface;
use crustacea::source::Source;
use crustacea::source::TextureBlending;
use crustacea::Surface;
use crustacea::Mapping;

const SCREEN_WIDTH: u32 = 800;
const SCREEN_HEIGHT: u32 = 600;

// ----------------------------------------------------------------------------

fn find_sdl_gl_driver() -> Option<u32> {
    for (index, item) in sdl2::render::drivers().enumerate() {
        if item.name == "opengl" {
            return Some(index as u32);
        }
    }
    None
}

// ----------------------------------------------------------------------------

fn main() {

    let sdl_context = sdl2::init().unwrap();
    let video_subsys = sdl_context.video().unwrap();
    let mut window = video_subsys.window("Crustacea", 
        SCREEN_WIDTH, SCREEN_HEIGHT)
        .position_centered()
        .opengl()
        .build()
        .unwrap();

    // window.set_fullscreen(FullscreenType::Desktop);

    // let mut canvas = window.into_canvas().build().unwrap();
    let mut canvas = window.into_canvas()
        .index(find_sdl_gl_driver().unwrap())
        .build()
        .unwrap();

    canvas.set_draw_color(pixels::Color::RGB(0, 0, 0));
    canvas.clear();
    canvas.present();

    // Mapping
    let points = vec![Point::new(100, 100),
                    Point::new(200, 100),
                    Point::new(200, 200),
                    Point::new(100, 250)];
    let mut surface = Surface::new(String::from("S1"), TextureBlending::new(), 
        points);

    let mut mapping = Mapping::new();
    mapping.add_surface(String::from("surface1"), surface);


    let mut last = Point::new(0, 0);
    let mut mouse_held = false;

    let mut events = sdl_context.event_pump().unwrap();

    'main: loop {

        canvas.set_draw_color(pixels::Color::RGB(0, 0, 0));
        canvas.clear();

        for event in events.poll_iter() {    
            match event {                
                Event::Quit {..} => {
                    break 'main
                }

                Event::MouseMotion {x, y, ..} => {
                    if mouse_held {
                        surface::offset_selected(mapping.surfaces(), 
                            &Point::new(-last.x() + x, -last.y() + y));
                    }
                    last = Point::new(x, y);
                }

                Event::MouseButtonDown {x, y, ..} => {
                    mouse_held = true;
                    surface::deselect_all(mapping.surfaces());
                    surface::select_at(mapping.surfaces(), &Point::new(x, y),
                        &Surface::hits_plane);
                }

                Event::MouseButtonUp {x, y, ..} => {
                    mouse_held = false;
                }

                _ => {}
            }
        }        
        
        for surface in mapping.surfaces() {
            gfx::draw(&canvas, surface);
        }
        canvas.present();

        std::thread::sleep(Duration::from_millis(25));
    }

}